package sg.ncs.product.pp_wrold_99.common.exception;

public class RemoteApiException extends RuntimeException {

    public RemoteApiException(String message) {
        super(message);
    }
}
